<?php
session_start();
error_reporting(E_ALL^E_NOTICE^E_WARNING);
if (isset($_GET['name'])) {
    $name = $_GET['name'];
}

if (!isset($_SESSION['TestSession'])) {
	$_SESSION['TestSession']=$name;
	$_SESSION['time']=time();
}

if (isset($_SESSION['TestSession']) && (time() - $_SESSION['time'] > 900)) {
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
	echo '<div class="warning">Connection Timed Out ！</div>';
	echo "<script>
		setTimeout(function(){window.location.href='index.php';},500);
		</script>";
}
?>