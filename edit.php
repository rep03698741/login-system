<title>Edit Message</title>
<?php
include "session.php";
include 'style.html';
if (isset($_GET['no'])) {
    $no = $_GET['no'];
    $time = $_GET['time'];
}
else{
    $no = $_POST['no'];
    $time = $_POST['time'];
}
?>

<body>
    <div class="flex-center position-ref full-height">
        <div class="top-right home">
            <a href='view.php?name=<?=$_SESSION['time']?>'>View</a>
            <!--<a href='upload.php?name=<?=$_SESSION['TestSession']?>'>Upload files</a>-->
            <!--a href="file.php">Files Download</a>;-->
            <a href="logout.php">Logout</a>
        </div>

<?php
include 'db.php';
if (!isset($_POST['submit'])) {
    $query = "select * from guestbook where no='$no'"; //選出該位使用者所留下的所有留言
    $query2 = 'select * from files where guestno="'.$no.'" and username= "'.$_SESSION['TestSession'].'" ';
    $result = mysqli_query($db, $query);
    $result2 = mysqli_query($db, $query2);

    foreach ($result as $one) {
?>

    <div class="content">
        <div class="m-b-md">
            <form name="form1" action="edit.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="no" value="<?=$one['no']?>">
                <input type="hidden" name="name" value="<?=$_SESSION['TestSession']?>">
                <input type="hidden" name="time" value="<?=$one['time']?>">
                <p style="background-color:powderblue;">SUBJECT</p>
                <input type="text" name="subject" style="background-color:blue" value="<?=$one['subject']?>">
                <p style="background-color:powderblue;">CONTENT</p>
                <textarea style="font-family: 'Nunito', sans-serif; font-size:20px; width:550px; height:100px; background-color:blue;" name="content"><?=$one['content']?></textarea>
                
                <?php
                foreach ($result2 as $two) {
                    echo "<br>File Name：" . $two['filesname'];
                }
                ?>

                <p><input type="file" name="file[]" multiple /><br /></p>
                <p><input type="submit" name="submit" value="SAVE">
                <style>
                    input {padding:5px 15px; background:#ccc; border:0 none;
                    cursor:pointer;
                    -webkit-border-radius: 5px;
                    border-radius: 5px; }
                </style>
                    <input type="reset" name="Reset" value="REWRITE">
                <style>
                    input {
                        padding:5px 15px;
                        background:rgb(46, 202, 189);
                        border:0 none;
                        cursor:pointer;
                        -webkit-border-radius: 5px;
                        border-radius: 5px;
                        font-family: 'Nunito', sans-serif;
                        font-size: 19px;
                    }
                </style>
            </form>
        </div>
</body>
</html>
<?php
    }
}

if (isset($_POST['submit'])) {
    include 'db.php';
	$no = $_POST['no'];
    $time = $_POST['time'];
	$subject = $_POST['subject'];
	$content = $_POST['content'];
    $query2 = 'select * from files where time="'.$time.'" and username= "'.$_SESSION['TestSession'].'" ';
    $result2 = mysqli_query($db, $query2);

	$sql = "update guestbook set subject='$subject',content='$content' where no='$no'";
	if (!mysqli_query($db, $sql)) {
		die(mysqli_connect_error());
	} else {
        $count=count($_FILES['file']['name']);
        for($i = 0; $i < $count; $i++){
            $fileName = $_FILES['file']['name'][$i];
            $fileTmpName = $_FILES['file']['tmp_name'][$i];
            $fileSize = $_FILES['file']['size'][$i];
            $fileError = $_FILES['file']['error'][$i];
            $fileType = $_FILES['file']['type'][$i];

            $fileExt = explode('.',$fileName);
            #$user = explode('@',$_SESSION['TestSession']);

            $fileActualExt = strtolower(end($fileExt));

            $allowed = array('pdf','doc','docx','jpg','jpeg','png');
            if(empty($fileName) || in_array($fileActualExt, $allowed)){
                if(empty($fileName) || $fileError === 0){
                    if($fileSize < 10000000){
                        if(!empty($fileName)){
                            #$fileNameNew =  $user[0] . "_" . $fileName;
                            $fileNameNew =  time() . "_" . $fileName;
                            $fileDestination = 'upload/'.$fileNameNew;
                            move_uploaded_file($fileTmpName, $fileDestination);
                            $sql = "INSERT files(username, filesname, size, downloads, time, guestno) VALUES ('{$_SESSION['TestSession']}', '$fileNameNew', '$fileSize', 0, now(), '$no')";

                            if (!mysqli_query($db, $sql)) {
                                die(mysqli_connect_error());
                            }
                        }
                    }else{
                        echo '<div class="warning">File is too large !</div>';
                        $_SESSION['time']=time();
                        echo "<script>
                        setTimeout(function(){window.location.href='board.php?name=" . $_SESSION['TestSession'] . "';},2000);
                        </script>";
                    }
                }else{
                    echo '<div class="warning">Error File Upload !</div>';
                    $_SESSION['time']=time();
                    echo "<script>
                    setTimeout(function(){window.location.href='board.php?name=" . $_SESSION['TestSession'] . "';},2000);
                    </script>";
                }
            }else{
                echo '<div class="warning">Error File Type !</div>';
                $_SESSION['time']=time();
                echo "<script>
                setTimeout(function(){window.location.href='board.php?name=" . $_SESSION['TestSession'] . "';},2000);
                </script>";
            }
        }
        $_SESSION['time']=time();
        echo '<div class="warning">Edit successfully ！</div>';
		echo "<script>
            setTimeout(function(){window.location.href='view.php?name=" . $_SESSION['TestSession'] . "';},500);
            </script>";
	}
} else {
	echo '<div class="success">Click <strong>Save</strong> when you\'re done.</div>';
}
?>