<?php

include 'session.php';
session_unset();     // unset $_SESSION variable for the run-time 
session_destroy();   // destroy session data in storage

echo "
    <script>
    setTimeout(function(){window.location.href='index.php';},500);
    </script>";
?>