<title>All messages</title>
<?php
include 'style.html';
include 'session.php';
?>

<body>
	<div class="flex-center position-ref full-height">
	<div class="top-right home">

<?php
if (!$_SESSION['TestSession']) {
	echo '<a href="index.php">Log in</a>';
	echo '<a href="register.php">Register</a>';
} else {
	echo "<a href='board.php?name=" . $_SESSION['TestSession'] . "'>Write some messages</a>";
	#echo '<a href="upload.php">Upload files</a>';
	#echo '<a href="file.php">Files Download</a>';
	echo '<a href="logout.php">Log out</a>';	
}
?>
    </div>
	</div>
<div class="note full-height">
<?php
include "db.php";
$sql = "select * from guestbook";
$sql2 = "select * from files";
$result = mysqli_query($db, $sql);
$result2 = mysqli_query($db, $sql2);

foreach ($result as $one) {
	echo "<br>Visitor Name：" . $one['name'];
	echo "<br>Subject：" . $one['subject'];
	echo "<br>Content：" . nl2br($one['content']);
	foreach ($result2 as $two) {
		if ($one['name']==$two['username'] && $one['no']==$two['guestno']){
			echo "<br>File Name：" . $two['filesname'];
			echo "<br>File Size：" . round($two['size']/1024,3) . "KB";
			echo "<br>File Downloads：" . $two['downloads'];
			echo "<a href='view.php?no=" . $two['no'] . "'>Downloads</a>";
			echo "<br>Time：" . $two['time'] . "<br>";
		}

		if(isset($_GET['no'])){
			$no = $_GET['no'];
			$sql3 = "select * from files where no='$no'";
			$result3 = mysqli_query($db, $sql3);
			$row = mysqli_fetch_assoc($result3);
			$filepath = 'upload/'. $row['filesname'];
		
			if(file_exists($filepath)){
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Transfer-Encoding: binary');
				header('Content-Disposition: attachment; filename=' . basename($filepath));
				header('Expires: 0');
				header('Cache-Control: public');
				header('Pragma:public');
				header('Content-Length:' . filesize($filepath));
		
				ob_clean();
				readfile($filepath);
		
				$newCount = $row['downloads'] + 1;
		
				$update = "update files set downloads=$newCount where no='$no'";
		
				mysqli_query($db, $update);
			}
		}
	}
	if($_SESSION['TestSession'] == $one['name']){
		echo ' 
		<br><a href=" edit.php?no=' . $one['no'] . '&time=' . $one['time'] . '&name=' . $_SESSION['TestSession'] . '">Edit message content</a>
		&nbsp|&nbsp<a href="delete.php?no=' . $one['no'] . '">Delete the message</a><br>';
		echo "<hr>";
	}
}

echo "<br>";
echo '<div class="bottom left position-abs content">';
echo "There are " . mysqli_num_rows($result) . " messages.";
?>
</div>
</body>
</html>